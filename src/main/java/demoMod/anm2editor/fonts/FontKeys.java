package demoMod.anm2editor.fonts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import demoMod.anm2editor.utils.Preferences;
import demoMod.gdxform.helpers.FontHelper;

import java.util.function.Consumer;

public final class FontKeys {
    public static final String SIM_HEI_12 = "SimHei-12";
    public static final String SIM_HEI_14 = "SimHei-14";
    public static final String SIM_HEI_16 = "SimHei-16";
    public static final String SIM_HEI_18 = "SimHei-18";
    public static final String SIM_HEI_20 = "SimHei-20";

    public static void initialize() {
        initializeFont(SIM_HEI_12, "fonts/simhei.ttf", parameter -> {
            parameter.incremental = true;
            parameter.characters = "";
            parameter.size = 12;
        });
        initializeFont(SIM_HEI_14, "fonts/simhei.ttf", parameter -> {
            parameter.incremental = true;
            parameter.characters = "";
            parameter.size = 14;
        });
        initializeFont(SIM_HEI_16, "fonts/simhei.ttf", parameter -> {
            parameter.incremental = true;
            parameter.characters = "";
            parameter.size = 16;
        });
        initializeFont(SIM_HEI_18, "fonts/simhei.ttf", parameter -> {
            parameter.incremental = true;
            parameter.characters = "";
            parameter.size = 18;
        });
        initializeFont(SIM_HEI_20, "fonts/simhei.ttf", parameter -> {
            parameter.incremental = true;
            parameter.characters = "";
            parameter.size = 20;
        });
    }

    private static void initializeFont(String fontKey, String fontPath, Consumer<FreeTypeFontGenerator.FreeTypeFontParameter> fontStyle) {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal(fontPath));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontStyle.accept(parameter);
        BitmapFont font = generator.generateFont(parameter);
        int[] resolution = Preferences.getPreferences().getResolution();
        if (resolution != null) {
            font.getData().scaleX *= resolution[0] / 1600.0F;
            font.getData().scaleY *= resolution[1] / 900.0F;
        }
        if (font.getData().scaleX < 1) {
            font.getData().scaleX = 1.0F;
        }
        if (font.getData().scaleY < 1) {
            font.getData().scaleY = 1.0F;
        }
        FontHelper.registerFont(fontKey, font);
    }
}
