package demoMod.anm2editor.interfaces;

public interface ReloadStringsSubscriber {
    void onReloadStrings();
}
