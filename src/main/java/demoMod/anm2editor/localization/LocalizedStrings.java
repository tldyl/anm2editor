package demoMod.anm2editor.localization;

import com.badlogic.gdx.Gdx;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import demoMod.anm2editor.interfaces.ReloadStringsSubscriber;
import demoMod.anm2editor.utils.Preferences;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LocalizedStrings {
    private static Map<String, List<String>> strings;
    private static final List<ReloadStringsSubscriber> subscribers = new ArrayList<>();

    public static void reloadStrings() {
        String lan = Preferences.getPreferences().getLanguage();
        Gson gson = new Gson();

        strings = gson.fromJson(Gdx.files.internal("localizations/" + lan + ".json").readString("UTF-8"), new TypeToken<Map<String, List<String>>>(){}.getType());

        for (ReloadStringsSubscriber subscriber : subscribers) {
            subscriber.onReloadStrings();
        }
    }

    public static List<String> getStrings(String key) {
        return strings.get(key);
    }

    public static void subscribe(ReloadStringsSubscriber subscriber) {
        if (!subscribers.contains(subscriber)) {
            subscribers.add(subscriber);
        }
    }

    public static void unsubscribe(ReloadStringsSubscriber subscriber) {
        subscribers.remove(subscriber);
    }
}
