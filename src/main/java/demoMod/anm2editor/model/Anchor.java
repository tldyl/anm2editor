package demoMod.anm2editor.model;

import java.util.List;

/**
 * 锚点，x和y的取值范围都在[0, 1]
 */
public class Anchor {
    private List<Anchor> bezierPoints;

    private float x;
    private float y;

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public List<Anchor> getBezierPoints() {
        return bezierPoints;
    }

    public void setBezierPoints(List<Anchor> bezierPoints) {
        this.bezierPoints = bezierPoints;
    }
}
