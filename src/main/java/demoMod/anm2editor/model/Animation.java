package demoMod.anm2editor.model;

public class Animation {
    private String name;
    private int animationLength = 1;
    private boolean loop;
    private boolean defaultAnimation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAnimationLength() {
        return animationLength;
    }

    public void setAnimationLength(int animationLength) {
        this.animationLength = animationLength;
        if (this.animationLength < 1) {
            this.animationLength = 1;
        }
    }

    public boolean isLoop() {
        return loop;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public boolean isDefaultAnimation() {
        return defaultAnimation;
    }

    public void setDefaultAnimation(boolean defaultAnimation) {
        this.defaultAnimation = defaultAnimation;
    }
}
