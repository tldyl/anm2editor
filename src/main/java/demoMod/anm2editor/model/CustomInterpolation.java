package demoMod.anm2editor.model;

import com.badlogic.gdx.math.Interpolation;
import demoMod.anm2editor.enums.CustomCurveType;

import java.util.ArrayList;
import java.util.List;

public class CustomInterpolation extends Interpolation {
    /**
     * 指定了曲线的类型
     * @see CustomCurveType
     */
    private CustomCurveType curveType;

    private Anchor startingAnchor;

    private Anchor endingAnchor;

    private final List<Anchor> anchors = new ArrayList<>();

    @Override
    public float apply(float a) {
        return curveType.apply(getAnchors(), a);
    }

    public CustomCurveType getCurveType() {
        return curveType;
    }

    public void setCurveType(CustomCurveType curveType) {
        this.curveType = curveType;
    }

    public void setStartingAnchor(Anchor startingAnchor) {
        this.startingAnchor = startingAnchor;
    }

    public void setEndingAnchor(Anchor endingAnchor) {
        this.endingAnchor = endingAnchor;
    }

    public void addAnchor(Anchor anchor) {
        anchors.add(anchor);
    }

    public void removeAnchor(Anchor anchor) {
        this.anchors.remove(anchor);
    }

    public List<Anchor> getAnchors() {
        List<Anchor> ret = new ArrayList<>();
        ret.add(startingAnchor);
        ret.addAll(anchors);
        ret.add(endingAnchor);
        return ret;
    }

    public void clearAnchors() {
        anchors.clear();
    }
}
