package demoMod.anm2editor.model;

import demoMod.anm2editor.enums.CustomCurveType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CustomInterpolationSave implements Serializable {
    private static final long serialVersionUID = 0L;

    private CustomCurveType curveType;

    private Anchor startingAnchor;

    private Anchor endingAnchor;

    private List<Anchor> anchors = new ArrayList<>();

    public CustomCurveType getCurveType() {
        return curveType;
    }

    public void setCurveType(CustomCurveType curveType) {
        this.curveType = curveType;
    }

    public Anchor getStartingAnchor() {
        return startingAnchor;
    }

    public void setStartingAnchor(Anchor startingAnchor) {
        this.startingAnchor = startingAnchor;
    }

    public Anchor getEndingAnchor() {
        return endingAnchor;
    }

    public void setEndingAnchor(Anchor endingAnchor) {
        this.endingAnchor = endingAnchor;
    }

    public List<Anchor> getAnchors() {
        return anchors;
    }

    public void setAnchors(List<Anchor> anchors) {
        this.anchors = anchors;
    }
}
