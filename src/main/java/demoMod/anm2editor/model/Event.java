package demoMod.anm2editor.model;

import java.util.List;
import java.util.Map;

public class Event {
    private int id;
    private String name;
    private Map<String, List<Integer>> usages;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, List<Integer>> getUsages() {
        return usages;
    }

    public void setUsages(Map<String, List<Integer>> usages) {
        this.usages = usages;
    }
}
