package demoMod.anm2editor.model;

import com.badlogic.gdx.graphics.Color;

public class KeyFrame {
    public float xPosition;
    public float yPosition;
    public float xPivot;
    public float yPivot;
    public int xCrop;
    public int yCrop;
    public int width;
    public int height;
    public float xScale;
    public float yScale;
    public int delay;
    public boolean visible = true;
    public Color tint;
    public Color colorOffset;
    public float rotation;
    public boolean interpolated;
    public transient boolean selected = false;

    public KeyFrame makeCopy() {
        KeyFrame ret = new KeyFrame();
        ret.xPosition = this.xPosition;
        ret.yPosition = this.yPosition;
        ret.xPivot = this.xPivot;
        ret.yPivot = this.yPivot;
        ret.xCrop = this.xCrop;
        ret.yCrop = this.yCrop;
        ret.width = this.width;
        ret.height = this.height;
        ret.xScale = this.xScale;
        ret.yScale = this.yScale;
        ret.delay = this.delay;
        ret.visible = this.visible;
        ret.tint = this.tint.cpy();
        ret.colorOffset = this.colorOffset.cpy();
        ret.rotation = this.rotation;
        ret.interpolated = this.interpolated;
        return ret;
    }

    @Override
    public String toString() {
        return "KeyFrame{" +
                "xPosition=" + xPosition +
                ", yPosition=" + yPosition +
                ", xPivot=" + xPivot +
                ", yPivot=" + yPivot +
                ", xCrop=" + xCrop +
                ", yCrop=" + yCrop +
                ", width=" + width +
                ", height=" + height +
                ", xScale=" + xScale +
                ", yScale=" + yScale +
                ", delay=" + delay +
                ", visible=" + visible +
                ", tint=" + tint +
                ", colorOffset=" + colorOffset +
                ", rotation=" + rotation +
                ", interpolated=" + interpolated +
                ", selected=" + selected +
                '}';
    }
}
