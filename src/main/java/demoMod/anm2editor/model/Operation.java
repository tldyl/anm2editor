package demoMod.anm2editor.model;

public abstract class Operation {
    private Operation next;
    private Operation prev;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Operation getNext() {
        return next;
    }

    public void setNext(Operation next) {
        this.next = next;
    }

    public Operation getPrev() {
        return prev;
    }

    public void setPrev(Operation prev) {
        this.prev = prev;
    }

    public abstract void undo();

    public abstract void redo();
}
