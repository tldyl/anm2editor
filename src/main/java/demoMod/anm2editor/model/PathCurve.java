package demoMod.anm2editor.model;

import demoMod.anm2editor.enums.CustomPathType;

import java.util.ArrayList;
import java.util.List;

public class PathCurve {
    private CustomPathType pathType;
    private Anchor startingAnchor;

    private Anchor endingAnchor;

    private final List<Anchor> anchors = new ArrayList<>();

    public Anchor apply(float a) {
        return pathType.apply(getAnchors(), a);
    }

    public CustomPathType getPathType() {
        return pathType;
    }

    public void setPathType(CustomPathType pathType) {
        this.pathType = pathType;
    }

    public void setStartingAnchor(Anchor startingAnchor) {
        this.startingAnchor = startingAnchor;
    }

    public void setEndingAnchor(Anchor endingAnchor) {
        this.endingAnchor = endingAnchor;
    }

    public void addAnchor(Anchor anchor) {
        anchors.add(anchor);
    }

    public void removeAnchor(Anchor anchor) {
        this.anchors.remove(anchor);
    }

    public List<Anchor> getAnchors() {
        List<Anchor> ret = new ArrayList<>();
        ret.add(startingAnchor);
        ret.addAll(anchors);
        ret.add(endingAnchor);
        return ret;
    }

    public void clearAnchors() {
        anchors.clear();
    }
}
