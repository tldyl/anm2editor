package demoMod.anm2editor.model;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglFileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;
import demoMod.anm2editor.fonts.FontKeys;
import demoMod.anm2editor.localization.LocalizedStrings;
import demoMod.anm2editor.ui.MessageBox;
import demoMod.gdxform.helpers.FontHelper;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SpriteSheet implements Disposable {
    private String path;
    private Texture sprite;
    private final Map<String, TextureAtlas.TextureAtlasData.Region> regionMap = new LinkedHashMap<>();

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Texture getSprite() {
        return sprite;
    }

    public void setSprite(Texture sprite) {
        this.sprite = sprite;
    }

    public void loadAtlasData() {
        String[] tokens = path.split("\\.");
        StringBuilder sb = new StringBuilder();
        for (int i=0;i<tokens.length - 1;i++) {
            if (sb.length() > 0) {
                sb.append('.');
            }
            sb.append(tokens[i]);
        }
        sb.append(".atlas");
        regionMap.clear();
        try {
            if (new File(Project.currentProject.getFilePath() + File.separator + sb).exists()) {
                TextureAtlas.TextureAtlasData atlasData = new TextureAtlas.TextureAtlasData(new LwjglFileHandle(Project.currentProject.getFilePath() + File.separator + sb, Files.FileType.Absolute),
                        new LwjglFileHandle(Project.currentProject.getFilePath(), Files.FileType.Absolute), false);
                for (TextureAtlas.TextureAtlasData.Region region : atlasData.getRegions()) {
                    regionMap.put(region.name, region);
                }
            } else if (new File(sb.toString()).exists()) {
                TextureAtlas.TextureAtlasData atlasData = new TextureAtlas.TextureAtlasData(new LwjglFileHandle(sb.toString(), Files.FileType.Absolute),
                        new LwjglFileHandle(Project.currentProject.getFilePath(), Files.FileType.Absolute), false);
                for (TextureAtlas.TextureAtlasData.Region region : atlasData.getRegions()) {
                    regionMap.put(region.name, region);
                }
            }
        } catch (GdxRuntimeException e) {
            regionMap.clear();
            List<String> strings = LocalizedStrings.getStrings("SpriteSheet");
            new MessageBox(strings.get(0), strings.get(1) + sb, FontHelper.getFont(FontKeys.SIM_HEI_14));
        }
    }

    public TextureAtlas.TextureAtlasData.Region getRegion(String name) {
        return regionMap.get(name);
    }

    public Map<String, TextureAtlas.TextureAtlasData.Region> getRegions() {
        return this.regionMap;
    }

    public boolean hasAtlasData() {
        return !regionMap.isEmpty();
    }

    @Override
    public void dispose() {
        if (this.sprite != null) {
            this.sprite.dispose();
        }
    }
}
