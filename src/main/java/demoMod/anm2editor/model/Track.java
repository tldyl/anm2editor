package demoMod.anm2editor.model;

import demoMod.anm2editor.enums.LayerType;

import java.util.HashMap;
import java.util.Map;

public class Track {
    private String name;
    private final LayerType layerType;
    private int id;
    private int spriteSheetId;
    private final Map<String, TrackContent> contents = new HashMap<>();

    public Track(LayerType layerType) {
        this.layerType = layerType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LayerType getLayerType() {
        return layerType;
    }

    public int getSpriteSheetId() {
        if (layerType == LayerType.NULL) return -1;
        return spriteSheetId;
    }

    public void setSpriteSheetId(int spriteSheetId) {
        this.spriteSheetId = spriteSheetId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void addContent(String animationName, TrackContent content) {
        this.contents.put(animationName, content);
    }

    public TrackContent getContent(String animationName) {
        return this.contents.get(animationName);
    }

    public void removeContent(String animationName) {
        this.contents.remove(animationName);
    }
}
