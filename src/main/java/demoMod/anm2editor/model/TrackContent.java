package demoMod.anm2editor.model;

import java.util.List;

public class TrackContent {
    private List<KeyFrame> keyFrames;
    private boolean visible = true;

    public List<KeyFrame> getKeyFrames() {
        return keyFrames;
    }

    public TrackContent setKeyFrames(List<KeyFrame> keyFrames) {
        this.keyFrames = keyFrames;
        return this;
    }

    public KeyFrame getKeyFrameAt(int index) {
        int len = 0;
        for (KeyFrame keyFrame : keyFrames) {
            if (len + keyFrame.delay > index) {
                return keyFrame;
            }
            len += keyFrame.delay;
        }
        return null;
    }

    public int getKeyframeStartIndex(KeyFrame keyFrame) {
        int ret = 0;
        do {
            if (getKeyFrameAt(ret) == keyFrame) {
                return ret;
            }
            ret++;
        } while (keyFrames.contains(keyFrame) && keyFrames.indexOf(keyFrame) < keyFrames.size() - 1);
        return -1;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
