package demoMod.anm2editor.ui;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.backends.lwjgl.LwjglFileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import demoMod.anm2editor.fonts.FontKeys;
import demoMod.anm2editor.interfaces.ReloadStringsSubscriber;
import demoMod.anm2editor.localization.LocalizedStrings;
import demoMod.gdxform.abstracts.ScrollBar;
import demoMod.gdxform.enums.GFrameWindowMode;
import demoMod.gdxform.enums.GFrameWindowStyle;
import demoMod.gdxform.helpers.FontHelper;
import demoMod.gdxform.ui.GFrame;
import demoMod.gdxform.ui.GScrollPane;
import demoMod.gdxform.ui.GVScrollBar;

import java.util.List;

public class AboutWindow extends GFrame implements ReloadStringsSubscriber {
    private final ScrollBar scrollBar;

    private String creditString;

    private final List<String> strings = LocalizedStrings.getStrings("AboutWindow");

    private final Music bgm = Gdx.audio.newMusic(new LwjglFileHandle("audio/music/XM Music.mp3", Files.FileType.Internal));

    public AboutWindow() {
        super(Gdx.graphics.getWidth() / 2.0F - 150.0F, Gdx.graphics.getHeight() / 2.0F - 150.0F, 300, 325, FontHelper.getFont(FontKeys.SIM_HEI_14), GFrameWindowStyle.CLOSE_BUTTON_ONLY, false);
        setWindowMode(GFrameWindowMode.MODAL);
        setTitle(strings.get(0));
        GFrame internalWindow = new GFrame(0, 0, getWidth(), getHeight() * 0.75F, FontHelper.getFont(FontKeys.SIM_HEI_14), GFrameWindowStyle.NONE, true);
        creditString = getCreditString();
        GScrollPane credits = new GScrollPane(0, 0, getWidth(), getHeight() * 0.75F, getWidth(), getHeight() * 1.55F) {
            @Override
            public void render(SpriteBatch sb) {
                super.render(sb);
                getFrameBuffer().begin();
                FontHelper.renderSmartText(sb, FontHelper.getFont(FontKeys.SIM_HEI_16), creditString, getSelfX(true) + getWidth() / 7.0F, getSelfY(true) + getPlainHeight() * getVScrollBar().getProgress(), Color.WHITE);
                sb.flush();
                getFrameBuffer().end();
            }
        };
        scrollBar = new GVScrollBar(0, 0, 0, credits.getHeight() / 2.0F);
        credits.setVScrollBar(scrollBar);
        internalWindow.addElement(credits);
        internalWindow.setBorderWidth(0);
        addElement(internalWindow);
        bgm.setLooping(true);
        bgm.setVolume(0.5F);
        bgm.play();
    }

    private String getCreditString() {
        StringBuilder stringBuilder = new StringBuilder();
        int i = 1;
        while (i < strings.size()) {
            String item = strings.get(i);
            stringBuilder.append("#y").append(item.replace(" ", " #y"));
            int amount = Integer.parseInt(strings.get(i + 1));
            for (int j=i + 2;j<i + 2 + amount;j++) {
                stringBuilder.append(" NL ").append(strings.get(j));
            }
            stringBuilder.append(" NL ").append(" NL ");
            i += 2 + amount;
        }
        return stringBuilder.toString();
    }

    @Override
    public void update() {
        super.update();
        float progress = scrollBar.getProgress();
        progress += Gdx.graphics.getDeltaTime() * 0.05F;
        while (progress > 1) {
            progress -= 1.0F;
        }
        scrollBar.setProgress(progress);
    }

    @Override
    public void render(SpriteBatch sb) {
        super.render(sb);
        FontHelper.renderFontCentered(sb, FontHelper.getFont(FontKeys.SIM_HEI_18), "Anm2 Animation Editor", getX(true) + getWidth() / 2.0F, getY(true) + getHeight() * 0.875F, Color.WHITE);
        FontHelper.renderFontCentered(sb, FontHelper.getFont(FontKeys.SIM_HEI_14), "v1.4.5", getX(true) + getWidth() / 2.0F, getY(true) + getHeight() * 0.8F, Color.WHITE);
        sb.flush();
    }

    @Override
    public void onReloadStrings() {
        List<String> strings = LocalizedStrings.getStrings("AboutWindow");
        this.strings.clear();
        this.strings.addAll(strings);
        this.creditString = getCreditString();
    }

    @Override
    public void dispose() {
        super.dispose();
        this.bgm.stop();
        this.bgm.dispose();
    }
}
