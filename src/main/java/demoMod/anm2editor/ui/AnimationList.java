package demoMod.anm2editor.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import demoMod.anm2editor.utils.Preferences;
import demoMod.gdxform.core.FormManager;
import demoMod.gdxform.ui.GList;
import demoMod.gdxform.ui.GScrollPane;

public class AnimationList extends GList<AnimationItem> {
    private boolean enabled = true;

    @Override
    public void update() {
        for (ListCellRenderer<AnimationItem> listCellRenderer : getElements()) {
            listCellRenderer.setWidth(getWidth());
            listCellRenderer.setHeight(((Timeline) FormManager.getInstance().getContainerById(Timeline.ID)).getTrackHeight());
        }
        super.update();
    }

    @Override
    public float getWidth() {
        if (getParent() instanceof GScrollPane) {
            return ((GScrollPane) getParent()).getPlainWidth();
        }
        return getParent() == null ? Gdx.graphics.getWidth() : getParent().getWidth();
    }

    @Override
    public boolean clickDown(int screenX, int screenY, int button) {
        if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) || Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT) || !enabled || Preferences.getPreferences().isPathEditMode()) return false;
        return super.clickDown(screenX, screenY, button);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        this.enabled = enabled;
    }
}
