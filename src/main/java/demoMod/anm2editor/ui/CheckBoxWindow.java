package demoMod.anm2editor.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import demoMod.anm2editor.localization.LocalizedStrings;
import demoMod.gdxform.core.FormManager;
import demoMod.gdxform.enums.GFrameWindowMode;
import demoMod.gdxform.enums.GFrameWindowStyle;
import demoMod.gdxform.ui.GButton;
import demoMod.gdxform.ui.GCheckBox;
import demoMod.gdxform.ui.GFrame;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class CheckBoxWindow extends GFrame {
    private Consumer<List<GCheckBox>> confirmCallback;
    private Consumer<List<GCheckBox>> cancelCallback;

    private final List<GCheckBox> checkBoxList = new ArrayList<>();

    public CheckBoxWindow(String title, List<GCheckBox> options, BitmapFont bannerFont) {
        super(Gdx.graphics.getWidth() / 2.0F - 100.0F, Gdx.graphics.getHeight() / 2.0F - 50.0F, 200, 70 + options.size() * 20, bannerFont, GFrameWindowStyle.BANNER_ONLY, false);
        setWindowMode(GFrameWindowMode.MODAL);
        setTitle(title);
        float y = 20 + options.size() * 20;
        for (GCheckBox checkBox : options) {
            checkBox.setX(20.0F);
            checkBox.setY(y);
            checkBox.setWidth(16);
            checkBox.setHeight(16);
            addElement(checkBox);
            checkBoxList.add(checkBox);
            y -= 20;
        }
        setResizable(false);
        List<String> strings = LocalizedStrings.getStrings("CheckBoxWindow");
        GButton confirm = new GButton(78, 4, 50, 20, bannerFont) {
            @Override
            public void onClick() {
                if (confirmCallback != null) {
                    confirmCallback.accept(checkBoxList);
                }
                FormManager.getInstance().removeContainer(CheckBoxWindow.this);
            }
        };
        confirm.setText(strings.get(0));
        confirm.setBackground(new Color(0, 47.0F / 255.0F, 147.0F / 255.0F, 1.0F));
        confirm.setBorderWidth(2);
        confirm.setBorder(Color.LIGHT_GRAY.cpy());
        GButton cancel = new GButton(138, 4, 50, 20, bannerFont) {
            @Override
            public void onClick() {
                if (cancelCallback != null) {
                    cancelCallback.accept(checkBoxList);
                }
                FormManager.getInstance().removeContainer(CheckBoxWindow.this);
            }
        };
        cancel.setText(strings.get(1));
        cancel.setBackground(Color.LIGHT_GRAY.cpy());
        cancel.setBorderWidth(2);
        cancel.setBorder(Color.LIGHT_GRAY.cpy());
        addElement(confirm);
        addElement(cancel);
    }

    public void setConfirmCallback(Consumer<List<GCheckBox>> confirmCallback) {
        this.confirmCallback = confirmCallback;
    }

    public void setCancelCallback(Consumer<List<GCheckBox>> cancelCallback) {
        this.cancelCallback = cancelCallback;
    }
}
