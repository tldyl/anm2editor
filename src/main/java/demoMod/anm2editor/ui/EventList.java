package demoMod.anm2editor.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import demoMod.gdxform.core.FormManager;
import demoMod.gdxform.ui.GList;
import demoMod.gdxform.ui.GScrollPane;

public class EventList extends GList<EventItem> {
    @Override
    public void update() {
        for (ListCellRenderer<EventItem> listCellRenderer : getElements()) {
            listCellRenderer.setWidth(getWidth());
            listCellRenderer.setHeight(((Timeline) FormManager.getInstance().getContainerById(Timeline.ID)).getTrackHeight());
        }
        super.update();
    }

    @Override
    public float getWidth() {
        if (getParent() instanceof GScrollPane) {
            return ((GScrollPane) getParent()).getPlainWidth();
        }
        return getParent() == null ? Gdx.graphics.getWidth() : getParent().getWidth();
    }

    @Override
    public boolean clickDown(int screenX, int screenY, int button) {
        if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) || Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT)) return false;
        return super.clickDown(screenX, screenY, button);
    }
}
