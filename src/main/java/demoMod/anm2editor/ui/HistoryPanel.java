package demoMod.anm2editor.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import demoMod.anm2editor.fonts.FontKeys;
import demoMod.anm2editor.interfaces.ReloadStringsSubscriber;
import demoMod.anm2editor.localization.LocalizedStrings;
import demoMod.anm2editor.model.Operation;
import demoMod.anm2editor.utils.Preferences;
import demoMod.gdxform.enums.GFrameCloseStrategy;
import demoMod.gdxform.enums.GFrameWindowStyle;
import demoMod.gdxform.helpers.FontHelper;
import demoMod.gdxform.ui.GFrame;
import demoMod.gdxform.ui.GScrollPane;
import demoMod.gdxform.ui.GVScrollBar;

public class HistoryPanel extends GFrame implements ReloadStringsSubscriber {
    public static final String ID = "HistoryPanel";

    private Operation rootOperation;
    private Operation currentOperation;
    private String operationHistory = "";

    private int currentOperations;
    private int gcThreshold = 0;

    public HistoryPanel() {
        super(Gdx.graphics.getWidth() * 0.7F, Gdx.graphics.getHeight() * 0.5F, Gdx.graphics.getWidth() * 0.1F, Gdx.graphics.getHeight() * 0.2F, FontHelper.getFont(FontKeys.SIM_HEI_14), GFrameWindowStyle.CLOSE_BUTTON_ONLY, false);
        setCloseStrategy(GFrameCloseStrategy.HIDDEN_ON_CLOSE);
        setResizable(false);
        setTitle(LocalizedStrings.getStrings(ID).get(0));
        setId(ID);
        GScrollPane scrollPane = new GScrollPane(0, 0, getWidth(), getHeight() - getBannerHeight(), getWidth(), getHeight() * 3) {
            @Override
            public void render(SpriteBatch sb) {
                super.render(sb);
                getFrameBuffer().begin();
                FontHelper.renderSmartText(sb, getBannerFont(), operationHistory, this.getX(true) + 10.0F, this.getY(true) + this.getPlainHeight() - 20.0F, Color.WHITE.cpy());
                sb.flush();
                getFrameBuffer().end();
            }
        };
        GVScrollBar vScrollBar = new GVScrollBar(0, 0, 10, scrollPane.getHeight() / 3.0F);
        scrollPane.setVScrollBar(vScrollBar);
        addElement(scrollPane);
        LocalizedStrings.subscribe(this);
    }

    public Operation getRootOperation() {
        return rootOperation;
    }

    public void setRootOperation(Operation rootOperation) {
        this.rootOperation = rootOperation;
        this.currentOperation = rootOperation;
        operationHistory = getOperationHistory();
        currentOperations = 0;
    }

    public Operation getCurrentOperation() {
        return currentOperation;
    }

    public void addOperation(Operation operation) {
        currentOperation.setNext(operation);
        operation.setPrev(currentOperation);
        currentOperation = operation;
        if (currentOperations >= Preferences.getPreferences().getMaxPreservedOperations()) {
            rootOperation = rootOperation.getNext();
            gcThreshold++;
            if (gcThreshold > 100) {
                System.gc();
                gcThreshold = 0;
            }
        } else {
            currentOperations++;
        }
        operationHistory = getOperationHistory();
    }

    public void undo(Operation operation) {
        while (currentOperation != operation && currentOperation.getPrev() != null) {
            currentOperation.undo();
            currentOperation = currentOperation.getPrev();
            currentOperations--;
        }
        operationHistory = getOperationHistory();
    }

    public void redo(Operation operation) {
        while (currentOperation != operation && currentOperation.getNext() != null) {
            currentOperation = currentOperation.getNext();
            currentOperation.redo();
            if (currentOperations >= Preferences.getPreferences().getMaxPreservedOperations()) {
                rootOperation = rootOperation.getNext();
                gcThreshold++;
                if (gcThreshold > 100) {
                    System.gc();
                    gcThreshold = 0;
                }
            } else {
                currentOperations++;
            }
        }
        operationHistory = getOperationHistory();
    }

    private String getOperationHistory() {
        StringBuilder sb = new StringBuilder();
        Operation operation = rootOperation;
        boolean flag = false;
        while (operation != null) {
            if (sb.length() > 0) {
                sb.append(" NL ");
            }
            String textColor = "";
            if (flag) {
                textColor = "#p";
            }
            if (operation == currentOperation) {
                flag = true;
                textColor = "#g";
            }
            sb.append(textColor).append(operation.getName().replace(" ", " " + textColor));
            operation = operation.getNext();
        }
        return sb.toString();
    }

    @Override
    public void onReloadStrings() {
        setTitle(LocalizedStrings.getStrings(ID).get(0));
    }
}
