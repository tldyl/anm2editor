package demoMod.anm2editor.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import demoMod.anm2editor.localization.LocalizedStrings;
import demoMod.gdxform.core.FormManager;
import demoMod.gdxform.enums.GFrameWindowMode;
import demoMod.gdxform.enums.GFrameWindowStyle;
import demoMod.gdxform.ui.GButton;
import demoMod.gdxform.ui.GFrame;
import demoMod.gdxform.ui.GLabel;

public class MessageBox extends GFrame {
    private Runnable confirmCallback;

    public MessageBox(String title, String msg, BitmapFont bannerFont) {
        super(Gdx.graphics.getWidth() / 2.0F - 200.0F, Gdx.graphics.getHeight() / 2.0F - 50.0F, 400, 100, bannerFont, GFrameWindowStyle.BANNER_ONLY, false);
        setWindowMode(GFrameWindowMode.MODAL);
        setTitle(title);
        GButton confirm = new GButton(175, 10, 50, 20, bannerFont) {
            @Override
            public void onClick() {
                if (confirmCallback != null) {
                    confirmCallback.run();
                }
                FormManager.getInstance().removeContainer(MessageBox.this);
            }
        };
        confirm.setText(LocalizedStrings.getStrings("MessageBox").get(0));
        confirm.setBackground(new Color(0, 47.0F / 255.0F, 147.0F / 255.0F, 1.0F));
        confirm.setBorderWidth(2);
        confirm.setBorder(Color.LIGHT_GRAY.cpy());
        GLabel message = new GLabel(13, 51, 400, 16, bannerFont);
        message.setText(msg);
        addElement(confirm);
        addElement(message);
    }

    public void setConfirmCallback(Runnable confirmCallback) {
        this.confirmCallback = confirmCallback;
    }
}
