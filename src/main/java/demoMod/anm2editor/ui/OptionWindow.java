package demoMod.anm2editor.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import demoMod.anm2editor.localization.LocalizedStrings;
import demoMod.gdxform.core.FormManager;
import demoMod.gdxform.enums.GFrameWindowMode;
import demoMod.gdxform.enums.GFrameWindowStyle;
import demoMod.gdxform.ui.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class OptionWindow extends GFrame {
    protected final List<GRadioButton> buttonGroup = new ArrayList<>();
    private Consumer<Integer> confirmCallback;
    private Consumer<Integer> cancelCallback;

    public OptionWindow(String title, List<String> options, BitmapFont bannerFont, int defaultSelectedIndex) {
        super(Gdx.graphics.getWidth() / 2.0F - 100.0F, Gdx.graphics.getHeight() / 2.0F - 50.0F, 200, Math.min(70 + options.size() * 20, Gdx.graphics.getHeight() / 2.0F), bannerFont, GFrameWindowStyle.BANNER_ONLY, false);
        setWindowMode(GFrameWindowMode.MODAL);
        setTitle(title);
        float y = options.size() * 20 > Gdx.graphics.getHeight() / 2.0F ? options.size() * 20 : options.size() * 20 + 20;
        for (String option : options) {
            GRadioButton radioButton = new GRadioButton(20.0F, y, 12, 12, bannerFont);
            radioButton.setText(option);
            radioButton.setGroup(buttonGroup);
            y -= 20;
        }
        if (options.size() * 20 > Gdx.graphics.getHeight() / 2.0F) {
            GScrollPane scrollPane = new GScrollPane(0, 26, getWidth(), getHeight() - getBannerHeight() - 26, getWidth(), Math.max(options.size() * 20, getHeight()) + 20);
            scrollPane.setVScrollBar(new GVScrollBar(0, 0, 10, scrollPane.getHeight() / 2.0F));
            for (GRadioButton radioButton : buttonGroup) {
                scrollPane.addElement(radioButton);
            }
            addElement(scrollPane);
        } else {
            for (GRadioButton radioButton : buttonGroup) {
                addElement(radioButton);
            }
        }
        if (!buttonGroup.isEmpty()) {
            buttonGroup.get(defaultSelectedIndex).setState(1);
        }
        setResizable(false);
        GButton confirm = new GButton(78, 4, 50, 20, bannerFont) {
            @Override
            public void onClick() {
                if (confirmCallback != null) {
                    int index = 0;
                    for (GRadioButton radioButton : buttonGroup) {
                        if (radioButton.getState() == 1) {
                            confirmCallback.accept(index);
                            break;
                        }
                        index++;
                    }
                }
                FormManager.getInstance().removeContainer(OptionWindow.this);
            }
        };
        List<String> strings = LocalizedStrings.getStrings("QuestionBox");
        confirm.setText(strings.get(0));
        confirm.setBackground(new Color(0, 47.0F / 255.0F, 147.0F / 255.0F, 1.0F));
        confirm.setBorderWidth(2);
        confirm.setBorder(Color.LIGHT_GRAY.cpy());
        GButton cancel = new GButton(138, 4, 50, 20, bannerFont) {
            @Override
            public void onClick() {
                if (cancelCallback != null) {
                    int index = 0;
                    for (GRadioButton radioButton : buttonGroup) {
                        if (radioButton.getState() == 1) {
                            cancelCallback.accept(index);
                            break;
                        }
                        index++;
                    }
                }
                FormManager.getInstance().removeContainer(OptionWindow.this);
            }
        };
        cancel.setText(strings.get(1));
        cancel.setBackground(Color.LIGHT_GRAY.cpy());
        cancel.setBorderWidth(2);
        cancel.setBorder(Color.LIGHT_GRAY.cpy());
        addElement(confirm);
        addElement(cancel);
    }

    public OptionWindow(String title, List<String> options, BitmapFont bannerFont) {
        this(title, options, bannerFont, 0);
    }

    public void setConfirmCallback(Consumer<Integer> confirmCallback) {
        this.confirmCallback = confirmCallback;
    }

    public void setCancelCallback(Consumer<Integer> cancelCallback) {
        this.cancelCallback = cancelCallback;
    }
}
