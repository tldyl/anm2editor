package demoMod.anm2editor.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import demoMod.anm2editor.localization.LocalizedStrings;
import demoMod.gdxform.core.FormManager;
import demoMod.gdxform.enums.GFrameWindowMode;
import demoMod.gdxform.enums.GFrameWindowStyle;
import demoMod.gdxform.ui.GButton;
import demoMod.gdxform.ui.GFrame;
import demoMod.gdxform.ui.GLabel;

import java.util.List;

public class QuestionBox extends GFrame {
    private Runnable confirmCallback;
    private Runnable cancelCallback;

    public QuestionBox(String title, String msg, BitmapFont bannerFont) {
        this(title, msg, bannerFont, LocalizedStrings.getStrings("QuestionBox"));
    }

    public QuestionBox(String title, String msg, BitmapFont bannerFont, List<String> strings) {
        super(Gdx.graphics.getWidth() / 2.0F - 200.0F, Gdx.graphics.getHeight() / 2.0F - 50.0F, 400, 100, bannerFont, GFrameWindowStyle.BANNER_ONLY, false);
        setWindowMode(GFrameWindowMode.MODAL);
        setTitle(title);
        GButton confirm = new GButton(120, 10, 60, 20, bannerFont) {
            @Override
            public void onClick() {
                if (confirmCallback != null) {
                    confirmCallback.run();
                }
                FormManager.getInstance().removeContainer(QuestionBox.this);
            }
        };
        confirm.setText(strings.get(0));
        confirm.setBackground(new Color(0, 47.0F / 255.0F, 147.0F / 255.0F, 1.0F));
        confirm.setBorderWidth(2);
        confirm.setBorder(Color.LIGHT_GRAY.cpy());
        GButton cancel = new GButton(220, 10, 60, 20, bannerFont) {
            @Override
            public void onClick() {
                if (cancelCallback != null) {
                    cancelCallback.run();
                }
                FormManager.getInstance().removeContainer(QuestionBox.this);
            }
        };
        cancel.setText(strings.get(1));
        cancel.setBackground(Color.LIGHT_GRAY.cpy());
        cancel.setBorderWidth(2);
        cancel.setBorder(Color.LIGHT_GRAY.cpy());
        GLabel message = new GLabel(13, 51, 4000, 32, bannerFont);
        message.setText(msg);
        addElement(confirm);
        addElement(cancel);
        addElement(message);
    }

    public void setConfirmCallback(Runnable confirmCallback) {
        this.confirmCallback = confirmCallback;
    }

    public void setCancelCallback(Runnable cancelCallback) {
        this.cancelCallback = cancelCallback;
    }
}
