package demoMod.anm2editor.ui;

import com.badlogic.gdx.Gdx;
import demoMod.anm2editor.fonts.FontKeys;
import demoMod.anm2editor.interfaces.ReloadStringsSubscriber;
import demoMod.anm2editor.localization.LocalizedStrings;
import demoMod.anm2editor.utils.Preferences;
import demoMod.gdxform.enums.GFrameWindowMode;
import demoMod.gdxform.enums.GFrameWindowStyle;
import demoMod.gdxform.helpers.FontHelper;
import demoMod.gdxform.ui.GCheckBox;
import demoMod.gdxform.ui.GFrame;

import java.util.List;

public class RegionOptionWindow extends GFrame implements ReloadStringsSubscriber {
    private final List<String> strings = LocalizedStrings.getStrings("RegionOptionWindow");

    public RegionOptionWindow() {
        super(Gdx.graphics.getWidth() / 2.0F - 200.0F, Gdx.graphics.getHeight() / 2.0F - 150.0F, 400, 325, FontHelper.getFont(FontKeys.SIM_HEI_14), GFrameWindowStyle.CLOSE_BUTTON_ONLY, false);
        setWindowMode(GFrameWindowMode.NON_MODAL);
        setTitle(strings.get(0));
        GCheckBox renameTrackAfterChangeRegion = new GCheckBox(getWidth() * 0.05F, getHeight() * 0.8F, 16, 16, FontHelper.getFont(FontKeys.SIM_HEI_14)) {
            @Override
            public void onClick() {
                Preferences.getPreferences().setRenameTrackAfterChangeRegion(getState() == 1);
                Preferences.save();
            }
        };
        renameTrackAfterChangeRegion.setId("renameTrackAfterChangeRegion");
        renameTrackAfterChangeRegion.setText(strings.get(1));
        renameTrackAfterChangeRegion.setState(Preferences.getPreferences().renameTrackAfterChangeRegion() ? 1 : 0);
        addElement(renameTrackAfterChangeRegion);
        LocalizedStrings.subscribe(this);
    }

    @Override
    public void onReloadStrings() {
        List<String> strings = LocalizedStrings.getStrings("RegionOptionWindow");
        this.strings.clear();
        this.strings.addAll(strings);
        setTitle(strings.get(0));
        ((GCheckBox) getElementById("renameTrackAfterChangeRegion")).setText(strings.get(1));
    }

    @Override
    public void dispose() {
        super.dispose();
        LocalizedStrings.unsubscribe(this);
    }
}
