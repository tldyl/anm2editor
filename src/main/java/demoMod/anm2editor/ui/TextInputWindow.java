package demoMod.anm2editor.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import demoMod.anm2editor.localization.LocalizedStrings;
import demoMod.gdxform.core.FormManager;
import demoMod.gdxform.enums.GFrameWindowMode;
import demoMod.gdxform.enums.GFrameWindowStyle;
import demoMod.gdxform.ui.GButton;
import demoMod.gdxform.ui.GFrame;
import demoMod.gdxform.ui.GTextField;

import java.util.List;
import java.util.function.Consumer;

public class TextInputWindow extends GFrame {
    private final GTextField textField;
    private Consumer<String> confirmCallback;
    private Consumer<String> cancelCallback;
    private final GButton confirm;
    private final GButton cancel;

    public TextInputWindow(String title, String defaultText, BitmapFont bannerFont) {
        super(Gdx.graphics.getWidth() / 2.0F - 100.0F, Gdx.graphics.getHeight() / 2.0F - 50.0F, 200, 100, bannerFont, GFrameWindowStyle.BANNER_ONLY, false);
        setWindowMode(GFrameWindowMode.MODAL);
        setTitle(title);
        textField = new GTextField(10, 30, 180, 30, bannerFont);
        textField.setText(defaultText);
        addElement(textField);
        setResizable(false);
        confirm = new GButton(78, 4, 50, 20, bannerFont) {
            @Override
            public void onClick() {
                if (confirmCallback != null) {
                    confirmCallback.accept(textField.getText());
                }
                FormManager.getInstance().removeContainer(TextInputWindow.this);
            }
        };
        List<String> strings = LocalizedStrings.getStrings("TextInputWindow");
        confirm.setText(strings.get(0));
        confirm.setBackground(new Color(0, 47.0F / 255.0F, 147.0F / 255.0F, 1.0F));
        confirm.setBorderWidth(2);
        confirm.setBorder(Color.LIGHT_GRAY.cpy());
        cancel = new GButton(138, 4, 50, 20, bannerFont) {
            @Override
            public void onClick() {
                if (cancelCallback != null) {
                    cancelCallback.accept(textField.getText());
                }
                FormManager.getInstance().removeContainer(TextInputWindow.this);
            }
        };
        cancel.setText(strings.get(1));
        cancel.setBackground(Color.LIGHT_GRAY.cpy());
        cancel.setBorderWidth(2);
        cancel.setBorder(Color.LIGHT_GRAY.cpy());
        addElement(confirm);
        addElement(cancel);
    }

    public void setConfirmCallback(Consumer<String> confirmCallback) {
        this.confirmCallback = confirmCallback;
    }

    public void setCancelCallback(Consumer<String> cancelCallback) {
        this.cancelCallback = cancelCallback;
    }

    public void setCharFilter(String regex) {
        textField.setCharFilter(regex);
    }

    @Override
    public boolean keyDown(int keyCode) {
        if (keyCode == Input.Keys.ENTER) {
            confirm.onClick();
        }
        return super.keyDown(keyCode);
    }
}
