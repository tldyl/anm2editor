package demoMod.anm2editor.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import demoMod.anm2editor.fonts.FontKeys;
import demoMod.gdxform.enums.GFrameCloseStrategy;
import demoMod.gdxform.enums.GFrameWindowStyle;
import demoMod.gdxform.helpers.FontHelper;
import demoMod.gdxform.ui.GFrame;
import demoMod.gdxform.ui.GRadioButton;

import java.util.ArrayList;
import java.util.List;

public class ToolBar extends GFrame {
    public static final String ID = "ToolBar";
    private final List<GRadioButton> group = new ArrayList<>();

    public ToolBar() {
        super(Gdx.graphics.getWidth() * 0.775F - 2, Gdx.graphics.getHeight() * 0.775F - 30 - 2, Gdx.graphics.getWidth() * 0.025F, Gdx.graphics.getWidth() * 0.125F + 10, FontHelper.getFont(FontKeys.SIM_HEI_14), GFrameWindowStyle.BANNER_ONLY, false);
        setResizable(false);
        setCloseStrategy(GFrameCloseStrategy.HIDDEN_ON_CLOSE);
        setId(ID);
        setBorderWidth(0);
        setBannerHeight(10);
        setBannerBackground(Color.LIGHT_GRAY.cpy());

        GRadioButton dragTool = new GRadioButton(0, getWidth() * 4.0F, getWidth(), getWidth());
        dragTool.setGroup(group);
        dragTool.setUnselectedTexture(new Texture("buttons/reset.png"));
        dragTool.setSelectedTexture(new Texture("buttons/reset_activated.png"));

        GRadioButton moveTool = new GRadioButton(0, getWidth() * 3.0F, getWidth(), getWidth());
        moveTool.setGroup(group);
        moveTool.setUnselectedTexture(new Texture("buttons/move.png"));
        moveTool.setSelectedTexture(new Texture("buttons/move_activated.png"));

        GRadioButton scaleTool = new GRadioButton(0, getWidth() * 2.0F, getWidth(), getWidth());
        scaleTool.setGroup(group);
        scaleTool.setUnselectedTexture(new Texture("buttons/scale.png"));
        scaleTool.setSelectedTexture(new Texture("buttons/scale_activated.png"));

        GRadioButton rotateTool = new GRadioButton(0, getWidth(), getWidth(), getWidth());
        rotateTool.setGroup(group);
        rotateTool.setUnselectedTexture(new Texture("buttons/rotate.png"));
        rotateTool.setSelectedTexture(new Texture("buttons/rotate_activated.png"));

        GRadioButton originTool = new GRadioButton(0, 0, getWidth(), getWidth());
        originTool.setGroup(group);
        originTool.setUnselectedTexture(new Texture("buttons/origin.png"));
        originTool.setSelectedTexture(new Texture("buttons/origin_activated.png"));
        dragTool.setState(1);
        addElement(dragTool);
        addElement(moveTool);
        addElement(scaleTool);
        addElement(rotateTool);
        addElement(originTool);
    }

    public int getSelectedIndex() {
        int index = 0;
        for (GRadioButton button : group) {
            if (button.getState() == 1) {
                return index;
            }
            index++;
        }
        return -1;
    }
}
