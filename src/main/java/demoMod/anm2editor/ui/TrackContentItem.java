package demoMod.anm2editor.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import demoMod.anm2editor.fonts.FontKeys;
import demoMod.anm2editor.localization.LocalizedStrings;
import demoMod.anm2editor.model.*;
import demoMod.anm2editor.utils.KeyFrameClipBoard;
import demoMod.gdxform.abstracts.Container;
import demoMod.gdxform.core.FormManager;
import demoMod.gdxform.helpers.FontHelper;
import demoMod.gdxform.interfaces.Element;
import demoMod.gdxform.interfaces.MouseEventSubscriber;
import demoMod.gdxform.ui.GList;
import demoMod.gdxform.ui.GMenuItem;
import demoMod.gdxform.ui.GMenuList;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TrackContentItem implements Element, MouseEventSubscriber {
    private Container<? extends Element> parent;
    private String id = "";
    private final Track track;
    private final List<KeyFrame> trackContent;
    private final Timeline timeline;
    private Texture cellTexture;
    private Color cellColor = Color.LIGHT_GRAY.cpy();
    private Color keyFrameBgColor = Color.SLATE.cpy();
    private Texture keyFrameBgTexture;
    private Color keyFrameSelectedColor = Color.TEAL.cpy();
    private Texture keyFrameSelectedTexture;
    private Color keyFrameBorderColor = Color.NAVY.cpy();
    private Texture keyFrameBorderTexture;
    private Texture interpolatedKeyFrameIcon;
    private Texture solidKeyFrameIcon;
    private KeyFrame currentSelectedKeyFrame = null;

    public TrackContentItem(String animationName, Track track) {
        refreshCellTexture();
        refreshKeyFrameBorderTexture();
        refreshKeyFrameBgTexture();
        refreshKeyFrameSelectedTexture();
        timeline = (Timeline) FormManager.getInstance().getContainerById(Timeline.ID);
        this.track = track;
        if (track.getContent(animationName) == null) {
            track.addContent(animationName, new TrackContent().setKeyFrames(new ArrayList<>()));
        }
        this.trackContent = track.getContent(animationName).getKeyFrames();
        for (KeyFrame keyFrame : trackContent) {
            keyFrame.selected = false;
        }
        Pixmap interpolatedKeyFrame = new Pixmap((int) timeline.getCellWidth(), (int) timeline.getTrackHeight(), Pixmap.Format.RGBA8888);
        interpolatedKeyFrame.setColor(Color.WHITE.cpy());
        interpolatedKeyFrame.fillCircle(interpolatedKeyFrame.getWidth() / 2, interpolatedKeyFrame.getHeight() / 2, (int) (interpolatedKeyFrame.getWidth() * 0.4F));
        interpolatedKeyFrameIcon = new Texture(interpolatedKeyFrame);

        Pixmap solidKeyFrame = new Pixmap((int) timeline.getCellWidth(), (int) timeline.getTrackHeight(), Pixmap.Format.RGBA8888);
        solidKeyFrame.setColor(Color.WHITE.cpy());
        solidKeyFrame.fillRectangle((int) (solidKeyFrame.getWidth() * 0.2F), (int) (solidKeyFrame.getHeight() - solidKeyFrame.getWidth() * 0.6F) / 2, (int) (solidKeyFrame.getWidth() * 0.6F), (int) (solidKeyFrame.getWidth() * 0.6F));
        solidKeyFrameIcon = new Texture(solidKeyFrame);
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public void update() {
        if (currentSelectedKeyFrame != null && !currentSelectedKeyFrame.selected) {
            currentSelectedKeyFrame = null;
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setColor(Color.WHITE);
        float cellX = timeline.getCellWidth();
        while (cellX <= getWidth()) {
            sb.draw(this.cellTexture, getX(true) + cellX, getY(true), 1, getHeight());
            cellX += timeline.getCellWidth();
        }
        if (this.trackContent != null) {
            cellX = 0.0F;
            for (KeyFrame keyFrame : this.trackContent) {
                sb.draw(this.keyFrameBorderTexture, getX(true) + cellX, getY(true), timeline.getCellWidth() * (float) keyFrame.delay, getHeight());
                if (keyFrame.selected) {
                    sb.draw(this.keyFrameSelectedTexture, getX(true) + cellX + 1.0F, getY(true) + 1.0F, timeline.getCellWidth() * (float) keyFrame.delay - 2.0F, getHeight() - 2.0F);
                } else {
                    sb.draw(this.keyFrameBgTexture, getX(true) + cellX + 1.0F, getY(true) + 1.0F, timeline.getCellWidth() * (float) keyFrame.delay - 2.0F, getHeight() - 2.0F);
                }
                if (keyFrame.interpolated) {
                    sb.draw(this.interpolatedKeyFrameIcon, getX(true) + cellX, getY(true), timeline.getCellWidth(), getHeight());
                } else {
                    sb.draw(this.solidKeyFrameIcon, getX(true) + cellX, getY(true), timeline.getCellWidth(), getHeight());
                }
                cellX += timeline.getCellWidth() * (float) keyFrame.delay;
            }
        }
    }

    @Override
    public Container<? extends Element> getParent() {
        return this.parent;
    }

    @Override
    public void setParent(Container<? extends Element> parent) {
        this.parent = parent;
    }

    @Override
    public float getX(boolean isAbsolute) {
        return getParent() != null ? getParent().getX(isAbsolute) : 0;
    }

    @Override
    public float getY(boolean isAbsolute) {
        return getParent() != null ? getParent().getY(isAbsolute) : 0;
    }

    @Override
    public void setX(float x) {

    }

    @Override
    public void setY(float y) {

    }

    @Override
    public float getWidth() {
        return getParent() != null ? getParent().getWidth() : 0;
    }

    @Override
    public float getHeight() {
        return getParent() != null ? getParent().getHeight() : 0;
    }

    @Override
    public void setWidth(float v) {

    }

    @Override
    public void setHeight(float v) {

    }

    @Override
    public Color getBackground() {
        return null;
    }

    @Override
    public void setBackground(Color color) {

    }

    public KeyFrame getCurrentSelectedKeyFrame() {
        return currentSelectedKeyFrame;
    }

    public Track getTrack() {
        return track;
    }

    public Color getCellColor() {
        return cellColor;
    }

    public void setCellColor(Color cellColor) {
        if (!this.cellColor.equals(cellColor)) {
            this.cellColor = cellColor;
            refreshCellTexture();
        }
    }

    private void refreshCellTexture() {
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(this.cellColor);
        pixmap.fill();
        if (this.cellTexture != null) {
            this.cellTexture.dispose();
        }
        this.cellTexture = new Texture(pixmap);
    }

    public void setInterpolatedKeyFrameIcon(Texture interpolatedKeyFrameIcon) {
        if (this.interpolatedKeyFrameIcon != null) {
            this.interpolatedKeyFrameIcon.dispose();
        }
        this.interpolatedKeyFrameIcon = interpolatedKeyFrameIcon;
    }

    public void setSolidKeyFrameIcon(Texture solidKeyFrameIcon) {
        if (this.solidKeyFrameIcon != null) {
            this.solidKeyFrameIcon.dispose();
        }
        this.solidKeyFrameIcon = solidKeyFrameIcon;
    }

    public Color getKeyFrameBgColor() {
        return keyFrameBgColor;
    }

    public void setKeyFrameBgColor(Color keyFrameBgColor) {
        if (!this.keyFrameBgColor.equals(keyFrameBgColor)) {
            this.keyFrameBgColor = keyFrameBgColor;
            refreshKeyFrameBgTexture();
        }
    }

    private void refreshKeyFrameBgTexture() {
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(this.keyFrameBgColor);
        pixmap.fill();
        if (this.keyFrameBgTexture != null) {
            this.keyFrameBgTexture.dispose();
        }
        this.keyFrameBgTexture = new Texture(pixmap);
    }

    public Color getKeyFrameBorderColor() {
        return keyFrameBorderColor;
    }

    public void setKeyFrameBorderColor(Color keyFrameBorderColor) {
        if (!this.keyFrameBorderColor.equals(keyFrameBorderColor)) {
            this.keyFrameBorderColor = keyFrameBorderColor;
            refreshKeyFrameBorderTexture();
        }
    }

    private void refreshKeyFrameBorderTexture() {
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(this.keyFrameBorderColor);
        pixmap.fill();
        if (this.keyFrameBorderTexture != null) {
            this.keyFrameBorderTexture.dispose();
        }
        this.keyFrameBorderTexture = new Texture(pixmap);
    }

    public void setKeyFrameSelectedColor(Color keyFrameSelectedColor) {
        if (!this.keyFrameSelectedColor.equals(keyFrameSelectedColor)) {
            this.keyFrameSelectedColor = keyFrameSelectedColor;
            refreshKeyFrameSelectedTexture();
        }
    }

    private void refreshKeyFrameSelectedTexture() {
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(this.keyFrameSelectedColor);
        pixmap.fill();
        if (this.keyFrameSelectedTexture != null) {
            this.keyFrameSelectedTexture.dispose();
        }
        this.keyFrameSelectedTexture = new Texture(pixmap);
    }

    @Override
    public boolean isResizable() {
        return false;
    }

    @Override
    public void setResizable(boolean b) {

    }

    @Override
    public boolean enabled() {
        return true;
    }

    @Override
    public void setEnabled(boolean b) {

    }

    @Override
    public boolean visible() {
        return true;
    }

    @Override
    public void setVisible(boolean b) {

    }

    @Override
    public void dispose() {
        if (this.cellTexture != null) {
            this.cellTexture.dispose();
        }
        if (this.keyFrameBgTexture != null) {
            this.keyFrameBgTexture.dispose();
        }
        if (this.keyFrameBorderTexture != null) {
            this.keyFrameBorderTexture.dispose();
        }
        if (this.interpolatedKeyFrameIcon != null) {
            this.interpolatedKeyFrameIcon.dispose();
        }
        if (this.solidKeyFrameIcon != null) {
            this.solidKeyFrameIcon.dispose();
        }
    }

    @Override
    public boolean clickDown(int screenX, int screenY, int button) {
        if (getParent() != null && getParent().getParent() instanceof TrackContentList && !((GList.ListCellRenderer<TrackContentItem>) getParent()).isSelected()) {
            int index = ((TrackContentList) getParent().getParent()).indexOf(this);
            ((TrackPane) FormManager.getInstance().getContainerById(TrackPane.ID)).selectTrack(index);
        } else if (((GList.ListCellRenderer<TrackContentItem>) getParent()).isSelected()) {
            ((TrackPane) FormManager.getInstance().getContainerById(TrackPane.ID)).deselect();
            this.currentSelectedKeyFrame = null;
        }
        if (screenX > timeline.getX(true) && screenX < timeline.getX(true) + timeline.getWidth() && Gdx.graphics.getHeight() - screenY > getY(true)
        && Gdx.graphics.getHeight() - screenY < getY(true) + getHeight()) {
            float x = getX(true);
            boolean selected = false;
            for (KeyFrame keyFrame : this.trackContent) {
                x += (float) keyFrame.delay * this.timeline.getCellWidth();
                if (screenX <= x) {
                    selected = true;
                    timeline.deselect();
                    if (this.currentSelectedKeyFrame != null) {
                        this.currentSelectedKeyFrame.selected = false;
                    }
                    this.currentSelectedKeyFrame = keyFrame;
                    keyFrame.selected = true;
                    ((KeyFramePropertyPanel) FormManager.getInstance().getContainerById(KeyFramePropertyPanel.ID)).refreshKeyFrameProperty();
                    break;
                }
            }
            if (!selected && this.currentSelectedKeyFrame != null) {
                this.currentSelectedKeyFrame.selected = false;
                this.currentSelectedKeyFrame = null;
            } else if (button == Input.Buttons.RIGHT) {
                GMenuList menuList = new GMenuList();
                menuList.setWidth(100.0F);
                menuList.setX(screenX);
                menuList.setY(Gdx.graphics.getHeight() - screenY);

                List<String> strings = LocalizedStrings.getStrings("TrackContentItem");
                SpriteSheet spriteSheet = Project.currentProject.getSpriteSheet(track.getSpriteSheetId());
                GMenuItem newFrame = new GMenuItem(menuList.getWidth(), menuList.getMenuItemHeight(), strings.get(0), FontHelper.getFont(FontKeys.SIM_HEI_14)) {
                    @Override
                    public void onClick() {
                        KeyFrame keyFrame = new KeyFrame();
                        Texture sprite = Project.currentProject.getSpriteSheet(track.getSpriteSheetId()).getSprite();
                        keyFrame.width = sprite.getWidth();
                        keyFrame.height = sprite.getHeight();
                        keyFrame.tint = Color.WHITE.cpy();
                        keyFrame.colorOffset = Color.WHITE.cpy();
                        keyFrame.xScale = 100.0F;
                        keyFrame.yScale = 100.0F;
                        keyFrame.delay = 1;
                        KeyFrame currentSelectedKeyFrame1 = TrackContentItem.this.currentSelectedKeyFrame;
                        if (TrackContentItem.this.currentSelectedKeyFrame != null) {
                            TrackContentItem.this.currentSelectedKeyFrame.selected = false;
                            TrackContentItem.this.trackContent.add(
                                    TrackContentItem.this.trackContent.indexOf(TrackContentItem.this.currentSelectedKeyFrame) + 1,
                                    keyFrame
                            );
                        } else {
                            TrackContentItem.this.trackContent.add(keyFrame);
                        }
                        TrackContentItem.this.currentSelectedKeyFrame = keyFrame;
                        keyFrame.selected = true;
                        int len = StatusBar.getMaxAnimationLength();
                        if (len > 120) {
                            while (timeline.getScrollPane().getPlainWidth() < len * timeline.getCellWidth()) {
                                timeline.getScrollPane().setPlainWidth(timeline.getScrollPane().getPlainWidth() + 50 * timeline.getCellWidth());
                            }
                        }
                        Operation operation = new Operation() {
                            @Override
                            public void undo() {
                                TrackContentItem.this.trackContent.remove(keyFrame);
                                int len = StatusBar.getMaxAnimationLength();
                                if (len > 120) {
                                    timeline.getScrollPane().setPlainWidth(timeline.getScrollPane().getPlainWidth() - KeyFrameClipBoard.keyFrame.delay * timeline.getCellWidth());
                                } else {
                                    timeline.getScrollPane().setPlainWidth(120 * timeline.getCellWidth());
                                }
                            }

                            @Override
                            public void redo() {
                                if (currentSelectedKeyFrame1 != null) {
                                    currentSelectedKeyFrame1.selected = false;
                                    TrackContentItem.this.trackContent.add(
                                            TrackContentItem.this.trackContent.indexOf(currentSelectedKeyFrame1),
                                            keyFrame
                                    );
                                } else {
                                    TrackContentItem.this.trackContent.add(keyFrame);
                                }
                                TrackContentItem.this.currentSelectedKeyFrame = keyFrame;
                                keyFrame.selected = true;
                                int len = StatusBar.getMaxAnimationLength();
                                if (len > 120) {
                                    while (timeline.getScrollPane().getPlainWidth() < len * timeline.getCellWidth()) {
                                        timeline.getScrollPane().setPlainWidth(timeline.getScrollPane().getPlainWidth() + 50 * timeline.getCellWidth());
                                    }
                                }
                            }
                        };
                        operation.setName(strings.get(1));
                        ((HistoryPanel) FormManager.getInstance().getContainerById(HistoryPanel.ID)).addOperation(operation);
                    }
                };
                GMenuItem copy = new GMenuItem(menuList.getWidth(), menuList.getMenuItemHeight(), strings.get(2), FontHelper.getFont(FontKeys.SIM_HEI_14)) {
                    @Override
                    public void onClick() {
                        KeyFrameClipBoard.keyFrame = TrackContentItem.this.currentSelectedKeyFrame;
                    }
                };
                copy.setEnabled(this.currentSelectedKeyFrame != null);
                GMenuItem cut = new GMenuItem(menuList.getWidth(), menuList.getMenuItemHeight(), strings.get(3), FontHelper.getFont(FontKeys.SIM_HEI_14)) {
                    @Override
                    public void onClick() {
                        KeyFrame target = TrackContentItem.this.currentSelectedKeyFrame;
                        int index = TrackContentItem.this.trackContent.indexOf(target);
                        KeyFrameClipBoard.keyFrame = TrackContentItem.this.currentSelectedKeyFrame;
                        TrackContentItem.this.trackContent.remove(KeyFrameClipBoard.keyFrame);
                        int len = StatusBar.getMaxAnimationLength();
                        if (len > 120) {
                            timeline.getScrollPane().setPlainWidth(timeline.getScrollPane().getPlainWidth() - KeyFrameClipBoard.keyFrame.delay * timeline.getCellWidth());
                        } else {
                            timeline.getScrollPane().setPlainWidth(120 * timeline.getCellWidth());
                        }
                        Operation operation = new Operation() {
                            @Override
                            public void undo() {
                                if (TrackContentItem.this.currentSelectedKeyFrame != null) {
                                    TrackContentItem.this.currentSelectedKeyFrame.selected = false;
                                }
                                TrackContentItem.this.trackContent.add(index, target);
                                TrackContentItem.this.currentSelectedKeyFrame = target;
                                target.selected = true;
                                int len = StatusBar.getMaxAnimationLength();
                                if (len > 120) {
                                    while (timeline.getScrollPane().getPlainWidth() < len * timeline.getCellWidth()) {
                                        timeline.getScrollPane().setPlainWidth(timeline.getScrollPane().getPlainWidth() + 50 * timeline.getCellWidth());
                                    }
                                }
                            }

                            @Override
                            public void redo() {
                                KeyFrameClipBoard.keyFrame = target;
                                TrackContentItem.this.trackContent.remove(KeyFrameClipBoard.keyFrame);
                                int len = StatusBar.getMaxAnimationLength();
                                if (len > 120) {
                                    timeline.getScrollPane().setPlainWidth(timeline.getScrollPane().getPlainWidth() - KeyFrameClipBoard.keyFrame.delay * timeline.getCellWidth());
                                } else {
                                    timeline.getScrollPane().setPlainWidth(120 * timeline.getCellWidth());
                                }
                            }
                        };
                        operation.setName(strings.get(4));
                        ((HistoryPanel) FormManager.getInstance().getContainerById(HistoryPanel.ID)).addOperation(operation);
                    }
                };
                cut.setEnabled(this.currentSelectedKeyFrame != null);
                GMenuItem paste = new GMenuItem(menuList.getWidth(), menuList.getMenuItemHeight(), strings.get(5), FontHelper.getFont(FontKeys.SIM_HEI_14)) {
                    @Override
                    public void onClick() {
                        if (KeyFrameClipBoard.keyFrame != null) {
                            KeyFrame keyFrameToAdd = KeyFrameClipBoard.keyFrame.makeCopy();
                            KeyFrame selectedKeyFrame = TrackContentItem.this.currentSelectedKeyFrame;
                            int index = TrackContentItem.this.trackContent.indexOf(selectedKeyFrame) + 1;
                            if (selectedKeyFrame != null) {
                                TrackContentItem.this.trackContent.add(index, keyFrameToAdd);
                            } else {
                                TrackContentItem.this.trackContent.add(TrackContentItem.this.trackContent.size(), keyFrameToAdd);
                            }
                            int len = StatusBar.getMaxAnimationLength();
                            if (len > 120) {
                                while (timeline.getScrollPane().getPlainWidth() < len * timeline.getCellWidth()) {
                                    timeline.getScrollPane().setPlainWidth(timeline.getScrollPane().getPlainWidth() + 50 * timeline.getCellWidth());
                                }
                            }
                            Operation operation = new Operation() {
                                @Override
                                public void undo() {
                                    TrackContentItem.this.trackContent.remove(keyFrameToAdd);
                                    int len = StatusBar.getMaxAnimationLength();
                                    if (len > 120) {
                                        timeline.getScrollPane().setPlainWidth(timeline.getScrollPane().getPlainWidth() - KeyFrameClipBoard.keyFrame.delay * timeline.getCellWidth());
                                    } else {
                                        timeline.getScrollPane().setPlainWidth(120 * timeline.getCellWidth());
                                    }
                                }

                                @Override
                                public void redo() {
                                    if (selectedKeyFrame != null) {
                                        TrackContentItem.this.trackContent.add(index, keyFrameToAdd);
                                    } else {
                                        TrackContentItem.this.trackContent.add(0, keyFrameToAdd);
                                    }
                                    int len = StatusBar.getMaxAnimationLength();
                                    if (len > 120) {
                                        while (timeline.getScrollPane().getPlainWidth() < len * timeline.getCellWidth()) {
                                            timeline.getScrollPane().setPlainWidth(timeline.getScrollPane().getPlainWidth() + 50 * timeline.getCellWidth());
                                        }
                                    }
                                }
                            };
                            operation.setName(strings.get(6));
                            ((HistoryPanel) FormManager.getInstance().getContainerById(HistoryPanel.ID)).addOperation(operation);
                        }
                    }
                };
                GMenuItem createSequence = new GMenuItem(menuList.getWidth(), menuList.getMenuItemHeight(), strings.get(7), FontHelper.getFont(FontKeys.SIM_HEI_14)) {
                    @Override
                    public void onClick() {
                        new TextInputWindow(strings.get(8), "0,0", FontHelper.getFont(FontKeys.SIM_HEI_14)).setConfirmCallback(pos -> {
                            float xOrigin = 0.0F;
                            float yOrigin = 0.0F;
                            boolean isOriginDataValid = false;
                            try {
                                if (pos.indexOf(",") > 0) {
                                    String[] tuple = pos.split(",");
                                    xOrigin = Float.parseFloat(tuple[0].trim());
                                    yOrigin = Float.parseFloat(tuple[1].trim());
                                    isOriginDataValid = true;
                                }
                            } catch (Exception ignored) {
                            }
                            List<KeyFrame> keyFrames = new ArrayList<>();
                            for (Map.Entry<String, TextureAtlas.TextureAtlasData.Region> e : spriteSheet.getRegions().entrySet()) {
                                TextureAtlas.TextureAtlasData.Region region = e.getValue();
                                KeyFrame keyFrame = new KeyFrame();
                                keyFrame.xCrop = region.left;
                                keyFrame.yCrop = region.top;
                                keyFrame.width = region.width;
                                keyFrame.height = region.height;
                                if (isOriginDataValid) {
                                    keyFrame.xPivot = xOrigin - region.offsetX;
                                    keyFrame.yPivot = region.height + region.offsetY - yOrigin;
                                } else {
                                    keyFrame.xPivot = region.width / 2.0F;
                                    keyFrame.yPivot = region.height / 2.0F;
                                }
                                if (region.rotate) {
                                    int tmp = keyFrame.width;
                                    keyFrame.width = keyFrame.height;
                                    keyFrame.height = tmp;
                                    keyFrame.rotation = 90;
                                    float tmpF = keyFrame.xPivot;
                                    keyFrame.xPivot = keyFrame.yPivot;
                                    keyFrame.yPivot = tmpF;
                                    keyFrame.yPivot += (region.originalWidth - region.width) / 2.0F - region.offsetX;
                                }
                                keyFrame.delay = 1;
                                keyFrame.xScale = 100.0F;
                                keyFrame.yScale = 100.0F;
                                keyFrame.tint = Color.WHITE.cpy();
                                keyFrame.colorOffset = Color.WHITE.cpy();
                                keyFrames.add(keyFrame);

                                TrackContentItem.this.trackContent.add(keyFrame);
                            }
                            Operation operation = new Operation() {
                                @Override
                                public void undo() {
                                    TrackContentItem.this.trackContent.removeAll(keyFrames);
                                }

                                @Override
                                public void redo() {
                                    TrackContentItem.this.trackContent.addAll(keyFrames);
                                }
                            };
                            operation.setName(strings.get(7));
                            ((HistoryPanel) FormManager.getInstance().getContainerById(HistoryPanel.ID)).addOperation(operation);
                        });
                    }
                };
                createSequence.setSplitLine(false);
                createSequence.setEnabled(spriteSheet.hasAtlasData());
                menuList.addElement(newFrame);
                menuList.addElement(copy);
                menuList.addElement(cut);
                menuList.addElement(paste);
                menuList.addElement(createSequence);
                FormManager.getInstance().addContainer(menuList);
                FormManager.getInstance().moveToTop(menuList);
            }
            return true;
        }
        if (this.currentSelectedKeyFrame != null) {
            this.currentSelectedKeyFrame.selected = false;
            this.currentSelectedKeyFrame = null;
        }
        return false;
    }

    @Override
    public boolean clickUp(int x, int y, int button) {
        return false;
    }

    @Override
    public boolean mouseDragged(int x, int y) {
        return false;
    }

    @Override
    public boolean mouseMoved(int x, int y) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public boolean moveToElementBorder(Element element) {
        return false;
    }
}
