package demoMod.anm2editor.ui;

import com.badlogic.gdx.Gdx;
import demoMod.gdxform.core.FormManager;
import demoMod.gdxform.ui.GList;
import demoMod.gdxform.ui.GScrollPane;

import java.util.List;

public class TrackContentList extends GList<TrackContentItem> {
    @Override
    public void update() {
        for (ListCellRenderer<TrackContentItem> listCellRenderer : getElements()) {
            listCellRenderer.setWidth(getWidth());
            listCellRenderer.setHeight(((Timeline) FormManager.getInstance().getContainerById(Timeline.ID)).getTrackHeight());
        }
        super.update();
    }

    @Override
    public float getWidth() {
        if (getParent() instanceof GScrollPane) {
            return ((GScrollPane) getParent()).getPlainWidth();
        }
        return getParent() == null ? Gdx.graphics.getWidth() : getParent().getWidth();
    }

    @Override
    public float getHeight() {
        if (getParent() instanceof GScrollPane) {
            return ((GScrollPane) getParent()).getPlainHeight();
        }
        return super.getHeight();
    }

    public void swap(int index1, int index2) {
        List<ListCellRenderer<TrackContentItem>> elements = getElements();
        int t = elements.get(index1).getPriority();
        elements.get(index1).setPriority(elements.get(index2).getPriority());
        elements.get(index2).setPriority(t);
        sortElements();
    }
}
