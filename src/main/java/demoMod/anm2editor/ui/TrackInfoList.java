package demoMod.anm2editor.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import demoMod.gdxform.core.FormManager;
import demoMod.gdxform.ui.GList;
import demoMod.gdxform.ui.GScrollPane;

import java.util.List;

public class TrackInfoList extends GList<TrackInfoItem> {
    @Override
    public void update() {
        for (ListCellRenderer<TrackInfoItem> listCellRenderer : getElements()) {
            listCellRenderer.setWidth(getWidth());
            listCellRenderer.setHeight(((Timeline) FormManager.getInstance().getContainerById(Timeline.ID)).getTrackHeight());
        }
        super.update();
    }

    @Override
    public float getWidth() {
        if (getParent() instanceof GScrollPane) {
            return ((GScrollPane) getParent()).getPlainWidth();
        }
        return getParent() == null ? Gdx.graphics.getWidth() : getParent().getWidth();
    }

    public void swap(int index1, int index2) {
        List<ListCellRenderer<TrackInfoItem>> elements = getElements();
        int t = elements.get(index1).getPriority();
        elements.get(index1).setPriority(elements.get(index2).getPriority());
        elements.get(index2).setPriority(t);
        sortElements();
    }

    @Override
    public boolean clickDown(int screenX, int screenY, int button) {
        if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) || Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT)) return false;
        return super.clickDown(screenX, screenY, button);
    }
}
