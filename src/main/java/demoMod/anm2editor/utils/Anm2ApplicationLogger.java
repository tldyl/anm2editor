package demoMod.anm2editor.utils;

import com.badlogic.gdx.ApplicationLogger;
import org.apache.logging.log4j.Logger;

public class Anm2ApplicationLogger implements ApplicationLogger {
    private final Logger log;

    public Anm2ApplicationLogger(Logger log) {
        this.log = log;
    }

    @Override
    public void log(String tag, String message) {
        log.info(tag + " : " + message);
    }

    @Override
    public void log(String tag, String message, Throwable exception) {
        log.error(tag + " : " + message, exception);
    }

    @Override
    public void error(String tag, String message) {
        log.error(tag + " : " + message);
    }

    @Override
    public void error(String tag, String message, Throwable exception) {
        log.error(tag + " : " + message, exception);
    }

    @Override
    public void debug(String tag, String message) {
        log.debug(tag + " : " + message);
    }

    @Override
    public void debug(String tag, String message, Throwable exception) {
        log.error(tag + " : " + message, exception);
    }
}
