package demoMod.anm2editor.utils;

import java.util.concurrent.atomic.AtomicInteger;

public class IdGenerator {
    private static final AtomicInteger trackId = new AtomicInteger(0);
    private static final AtomicInteger nullId = new AtomicInteger(0);
    private static final AtomicInteger eventId = new AtomicInteger(0);
    private static final AtomicInteger spriteSheetId = new AtomicInteger(0);

    public static int nextTrackId() {
        return trackId.getAndIncrement();
    }

    public static int nextNullId() {
        return nullId.getAndIncrement();
    }

    public static int nextEventId() {
        return eventId.getAndIncrement();
    }

    public static int nextSpriteSheetId() {
        return spriteSheetId.getAndIncrement();
    }

    public static void setTrackId(int id) {
        if (id < 0) id = 0;
        trackId.set(id);
    }

    public static void setNullId(int id) {
        if (id < 0) id = 0;
        nullId.set(id);
    }

    public static void setEventId(int id) {
        if (id < 0) id = 0;
        eventId.set(id);
    }

    public static void setSpriteSheetId(int id) {
        if (id < 0) id = 0;
        spriteSheetId.set(id);
    }

    public static void reset() {
        trackId.set(0);
        nullId.set(0);
        eventId.set(0);
        spriteSheetId.set(0);
    }
}
