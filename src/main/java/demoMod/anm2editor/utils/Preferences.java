package demoMod.anm2editor.utils;

import com.badlogic.gdx.Gdx;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;

public class Preferences {
    private static final Logger log = LogManager.getLogger(Preferences.class);

    private boolean displayGuide = false;
    private String lastOpenPath;
    private String lastSavePath;
    private int[] resolution;
    private String language;
    private boolean renameTrackAfterChangeRegion;
    private int maxPreservedOperations = 50;
    private transient boolean pathEditMode = false;

    private static Preferences inst;
    private static final Gson gson = new Gson();

    public static Preferences getPreferences() {
        if (inst == null) {
            inst = new Preferences();
        }
        return inst;
    }

    public boolean displayGuide() {
        return displayGuide;
    }

    public void setDisplayGuide(boolean displayGuide) {
        this.displayGuide = displayGuide;
    }

    public String getLastOpenPath() {
        return lastOpenPath;
    }

    public void setLastOpenPath(String lastOpenPath) {
        if (lastOpenPath != null && lastOpenPath.endsWith(File.separator)) {
            lastOpenPath = lastOpenPath.substring(0, lastOpenPath.length() - 1);
        }
        this.lastOpenPath = lastOpenPath;
    }

    public String getLastSavePath() {
        return lastSavePath;
    }

    public void setLastSavePath(String lastSavePath) {
        if (lastSavePath != null && lastSavePath.endsWith(File.separator)) {
            lastSavePath = lastSavePath.substring(0, lastSavePath.length() - 1);
        }
        this.lastSavePath = lastSavePath;
    }

    public int[] getResolution() {
        return resolution;
    }

    public void setResolution(int[] resolution) {
        this.resolution = resolution;
    }

    public String getLanguage() {
        return language == null ? "zhs" : language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean renameTrackAfterChangeRegion() {
        return renameTrackAfterChangeRegion;
    }

    public void setRenameTrackAfterChangeRegion(boolean renameTrackAfterChangeRegion) {
        this.renameTrackAfterChangeRegion = renameTrackAfterChangeRegion;
    }

    public int getMaxPreservedOperations() {
        return maxPreservedOperations;
    }

    public void setMaxPreservedOperations(int maxPreservedOperations) {
        this.maxPreservedOperations = maxPreservedOperations;
    }

    public boolean isPathEditMode() {
        return pathEditMode;
    }

    public void setPathEditMode(boolean pathEditMode) {
        this.pathEditMode = pathEditMode;
    }

    public static void save() {
        File file = new File("anm2config.json");
        if (!file.exists()) {
            try {
                file.createNewFile();
                log.info("Successfully created config file.");
            } catch (IOException e) {
                log.error("Exception occurred while saving preferences!", e);
            }
        }
        try {
            OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(file), StandardCharsets.UTF_8);
            writer.write(gson.toJson(inst));
            writer.flush();
            writer.close();
            log.info("Successfully saved preferences.");
        } catch (IOException e) {
            log.error("Exception occurred while saving preferences!", e);
        }
    }

    public static void load() {
        File file = new File("anm2config.json");
        if (file.exists()) {
            inst = gson.fromJson(Gdx.files.absolute(file.getAbsolutePath()).readString("UTF-8"), new TypeToken<Preferences>(){}.getType());
        }
    }

    public static Gson getGson() {
        return gson;
    }
}
